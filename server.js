var express = require('express'),
	path = require('path'),
    http = require('http'),
    project = require('./routes/projects');


var app = express();

app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.use(express.logger('dev'));  /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser()),
    app.use(express.static(path.join(__dirname, 'app')));
});
 

// Create Our Crud Method
app.get('/projects', project.findAll);
app.get('/projects/:id', project.findById);
app.post('/add', project.addProject);
app.put('/projects/:id', project.updateProject);
app.delete('/projects/:id', project.deleteProject);
 
http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});