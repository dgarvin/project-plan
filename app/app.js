var app = angular.module('app', ['ngRoute', 'appControllers', 'appServices', 'appDirectives', 'ngResource']);

var appServices = angular.module('appServices', []);
var appControllers = angular.module('appControllers', []);
var appDirectives = angular.module('appDirectives', []);

var options = {};
options.api = {};
options.api.base_url = "http://localhost:3000";

app.config(['$locationProvider', '$routeProvider', 
  function($location, $routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/project.list.html',
            controller: 'ProjectListCtrl'
        })
        .when('/projects/:id', {
		    templateUrl: 'views/project.detail.html',
		    controller: 'ProjectDetailCtrl'
		  })
        .when('/add', {
		    templateUrl: 'views/admin.project.create.html',
		    controller: 'AdminProjectCreateCtrl'
		  })
        .otherwise({
            redirectTo: '/'
        });
}]);