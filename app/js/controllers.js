appControllers.controller('ProjectListCtrl', ['$scope', '$sce', 'ProjectService',
    function ProjectListCtrl($scope, $sce, ProjectService) {

        $scope.projects = [];

        ProjectService.findAll().success(function(data) {
            for (var postKey in data) {
                data[postKey].content = $sce.trustAsHtml(data[postKey].content);
            }

            $scope.projects = data;            
        }).error(function(data, status) {
            console.log(status);
            console.log(data);
        });
    }
]);

appControllers.controller('ProjectDetailCtrl', ['$scope', '$routeParams', '$sce', 'ProjectService',
    function ProjectDetailCtrl($scope, $routeParams, $sce, ProjectService) {      

   	$scope.projects = {};
    var id = $routeParams.id;


    ProjectService.findById(id).success(function(data, status) {
        data.content = $sce.trustAsHtml(data.content);
        $scope.projects = data;
        console.log(status);
    }).error(function(data, status) {
        console.log(status);
        console.log(data);
    });
    }
]);

appControllers.controller('AdminProjectCreateCtrl', ['$scope', '$location', 'ProjectService',
    function AdminProjectCreateCtrl($scope, $location, ProjectService) {
    	

        $scope.save = function save(projects) {

            ProjectService.addProject(projects).success(function(data) {
				$scope.projects = {}; // clear the form so our user is ready to enter another
				$scope.projects = data;
				console.log(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
        }
    }
]);
