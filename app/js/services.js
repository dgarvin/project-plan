appServices.factory('ProjectService', function($http) {
    return {
        findById: function(id) {
            return $http.get(options.api.base_url + '/projects/' + id);
        },
        
        findAll: function() {
            return $http.get(options.api.base_url + '/projects/');
        },

        delete: function(id) {
            return $http.delete(options.api.base_url + '/projects/' + id);
        },

        addProject: function(post) {
            return $http.post(options.api.base_url + '/add');
        },

        update: function(post) {
            return $http.put(options.api.base_url + '/projects', {'projects': projects});
        }
    };
});